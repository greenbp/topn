### topN.sh testing

```
$ ./topN.sh
  Usage : ./topN.sh <N> <filename>
```


####  t2.large - 5.1G	/tmp/test/numbers.txt

```
 $ time ./topN.sh 20 /tmp/test/numbers.txt
 #!/bin/bash
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000#
 real	12m39.496s
 user	17m41.272s
 sys	0m13.760s
```

#### t2.large - 11G	/tmp/test/numbers_huge.txt
 
```
 $ time ./topN.sh 20 /tmp/test/numbers_huge.txt
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000
 1000000#
 real	33m10.648s
 user	40m54.788s
 sys	0m33.024s
```
