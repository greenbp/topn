#!/bin/bash

if [ $# -lt 2 ]
then
  echo "  Usage : ./topN.sh <N> <filename>"
  exit
fi

LC_ALL=C sort --parallel=8 -n -r $2 | head -$1
